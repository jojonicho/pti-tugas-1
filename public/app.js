const goalList = document.querySelector("#goals");
const form = document.querySelector("#add-goal");

var today = new Date();
document.getElementById('dtText').innerHTML = "Today is " + today + " Get to work!";

function renderGoal(doc) {
    let li = document.createElement('li');
    let name = document.createElement('span');
    let deadline = document.createElement('span');
    let del = document.createElement('div');
    del.setAttribute("class", "del");

    li.setAttribute('data-id', doc.id)
    name.textContent = doc.data().name;
    deadline.textContent = doc.data().deadline;
    del.textContent = 'x';

    li.appendChild(name);
    li.appendChild(deadline);
    li.appendChild(del);
    goalList.appendChild(li);

    // delete data
    del.addEventListener('click', (e) => {
        e.stopPropagation();
        let id = e.target.parentElement.getAttribute('data-id');
        db.collection('goals').doc(id).delete();
    })
}

// // fetch data
// db.collection('goals').orderBy('deadline').get().then((snapshot) => {
//     snapshot.docs.forEach(doc => {
//         renderGoal(doc);
//     });
// })

// real time listener
db.collection('goals').orderBy('deadline').onSnapshot(snapshot => {
    let changes = snapshot.docChanges();
    changes.forEach(change => {
        if (change.type == 'added') {
            renderGoal(change.doc);
        } else if (change.type == 'removed') {
            let li = goalList.querySelector('[data-id=' + change.doc.id + ']');
            goalList.removeChild(li);
        }
    })
})

// submit data
form.addEventListener('submit', (e) => {
    e.preventDefault();
    db.collection('goals').add({
        name: form.name.value,
        deadline: form.deadline.value,
    })
    form.name.value = '';
    form.deadline.value = '';
})
